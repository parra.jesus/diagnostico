<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('products')->insert([
            ['id' => 1, 'nombre' => "Producto 1", 'brand_id' => 1],
	    ['id' => 2, 'nombre' => "Producto 2", 'brand_id' => 2],
	    ['id' => 3, 'nombre' => "Producto 3", 'brand_id' => 3],
	    ['id' => 4, 'nombre' => "Producto 4", 'brand_id' => 4],
	    ['id' => 5, 'nombre' => "Producto 5", 'brand_id' => 5],
	    ['id' => 6, 'nombre' => "Producto 6", 'brand_id' => 1],
	    ['id' => 7, 'nombre' => "Producto 7", 'brand_id' => 2],
	    ['id' => 8, 'nombre' => "Producto 8", 'brand_id' => 3],
	    ['id' => 9, 'nombre' => "Producto 9", 'brand_id' => 4],
	    ['id' => 10, 'nombre' => "Producto 10", 'brand_id' => 3],
	    ['id' => 11, 'nombre' => "Producto 11", 'brand_id' => 4],
	    ['id' => 12, 'nombre' => "Producto 12", 'brand_id' => 5],
	    ['id' => 13, 'nombre' => "Producto 13", 'brand_id' => 6],
	    ['id' => 14, 'nombre' => "Producto 14", 'brand_id' => 7],
	    ['id' => 15, 'nombre' => "Producto 15", 'brand_id' => 7],
	    ['id' => 16, 'nombre' => "Producto 16", 'brand_id' => 8],
	    ['id' => 17, 'nombre' => "Producto 17", 'brand_id' => 9],
	    ['id' => 18, 'nombre' => "Producto 18", 'brand_id' => 10],
	    ['id' => 19, 'nombre' => "Producto 19", 'brand_id' => 10],
	    ['id' => 20, 'nombre' => "Producto 20", 'brand_id' => 10],
	    ['id' => 21, 'nombre' => "Producto 21", 'brand_id' => 1],
	    ['id' => 22, 'nombre' => "Producto 22", 'brand_id' => 3],
	    ['id' => 23, 'nombre' => "Producto 23", 'brand_id' => 4],
	    ['id' => 24, 'nombre' => "Producto 24", 'brand_id' => 5],
	    ['id' => 25, 'nombre' => "Producto 25", 'brand_id' => 6],
	    ['id' => 26, 'nombre' => "Producto 26", 'brand_id' => 7],
	    ['id' => 27, 'nombre' => "Producto 27", 'brand_id' => 8],
	    ['id' => 28, 'nombre' => "Producto 28", 'brand_id' => 9],
	    ['id' => 29, 'nombre' => "Producto 29", 'brand_id' => 10],
	    ['id' => 30, 'nombre' => "Producto 30", 'brand_id' => 1],
	    ['id' => 31, 'nombre' => "Producto 31", 'brand_id' => 2],
	    ['id' => 32, 'nombre' => "Producto 32", 'brand_id' => 3],
	    ['id' => 33, 'nombre' => "Producto 33", 'brand_id' => 4],
	    ['id' => 34, 'nombre' => "Producto 34", 'brand_id' => 5],
	    ['id' => 35, 'nombre' => "Producto 35", 'brand_id' => 6],
	    ['id' => 36, 'nombre' => "Producto 36", 'brand_id' => 7],
	    ['id' => 37, 'nombre' => "Producto 37", 'brand_id' => 8],
	    ['id' => 38, 'nombre' => "Producto 38", 'brand_id' => 9],
	    ['id' => 39, 'nombre' => "Producto 39", 'brand_id' => 1],
	    ['id' => 40, 'nombre' => "Producto 40", 'brand_id' => 1],
	    ['id' => 41, 'nombre' => "Producto 41", 'brand_id' => 1],
	    ['id' => 42, 'nombre' => "Producto 42", 'brand_id' => 1],
	    ['id' => 43, 'nombre' => "Producto 43", 'brand_id' => 3],
	    ['id' => 44, 'nombre' => "Producto 44", 'brand_id' => 4],
	    ['id' => 45, 'nombre' => "Producto 45", 'brand_id' => 5],
	    ['id' => 46, 'nombre' => "Producto 46", 'brand_id' => 6],
	    ['id' => 47, 'nombre' => "Producto 47", 'brand_id' => 1],
	    ['id' => 48, 'nombre' => "Producto 48", 'brand_id' => 2],
	    ['id' => 49, 'nombre' => "Producto 49", 'brand_id' => 3],
	    ['id' => 50, 'nombre' => "Producto 50", 'brand_id' => 4],
        ]);

    }
}
