<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class BrandsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('brands')->insert([
            ['id' => 1, 'name' => "Marca A"],
	    ['id' => 2, 'name' => "Marca B"],
	    ['id' => 3, 'name' => "Marca C"],
	    ['id' => 4, 'name' => "Marca D"],
	    ['id' => 5, 'name' => "Marca E"],
	    ['id' => 6, 'name' => "Marca F"],
	    ['id' => 7, 'name' => "Marca G"],
	    ['id' => 8, 'name' => "Marca H"],
	    ['id' => 9, 'name' => "Marca I"],
	    ['id' => 10, 'name' => "Marca J"],
        ]);

    }
}
