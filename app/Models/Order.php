<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    
    protected $fillable = ['date','total','created_at','updated_at'];

    public function order_product() {

    	return $this -> hasMany(Order_Product::class);
}
