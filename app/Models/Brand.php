<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    use HasFactory;
    
    protected $fillable = ['name','created_at','updated_at'];
    /*public $timestamps = false;*/

    public function Product() {

    	return $this -> hasMany(Product::class);
    }
}
