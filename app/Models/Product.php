<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    
    protected $primaryKey = 'id';
    protected $fillable = ['nombre','brand_id','created_at','updated_at'];
    /*public $timestamps = false;*/
     
    public function brand() {

    	return $this -> belongsTo(Brand::class);
    }
    	
    
    public function order_product() {

    	return $this -> hasMany(Order_Product::class);
    }

}
