<!DOCTYPE html>
<html>
<head>
    <title>Productos</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" ></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>

<style type="text/css">
.alert{
    padding:15px 0px 0px 0px;
}    
</style>

<body>
     
<div class="container">
    <div class="card mt-3">
        <div class="card-header text-center">
            <h3>Laravel 9 Ajax Producto</h3>
        </div>
        <div class="card-body">
  
            <table class="table table-bordered mt-3">
                <tr>
                    <th colspan="3">
                        Productos
                        <button type="button" class="btn btn-success float-end btn-sm" data-bs-toggle="modal" data-bs-target="#postModal">
                            Crear un Producto
                        </button>
                    </th>
                </tr>
                <tr>
                    <th>Nro</th>
                    <th>Producto</th>
                    <th>Rama</th>
                </tr>
                @foreach($products as $product)
                    <tr>
                        <td>{{ $product->id }}</td>
                        <td>{{ $product->nombre }}</td>
                        <td>{{ $product->brand_id }}</td>
                    </tr>
                @endforeach
            </table>
  
        </div>
    </div>
</div>
  
<!-- Modal -->
<div class="modal fade" id="postModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Crear Producto</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">

                <form>
                    <div class="alert alert-danger print-error-msg" style="display:none">
                        <ul></ul>
                    </div>
        
                    <div class="mb-3">
                        <label for="titleID" class="form-label">Nombre:</label>
                        <input type="text" id="titleID" name="nombre" class="form-control" placeholder="Nombre" required="">
                    </div>
      
                    <div class="mb-3">
                        <label for="bodyID" class="form-label">Rama:</label>
                        <textarea name="brand_id" class="form-control" id="bodyID" placeholder="Rama"></textarea>
                    </div>
         
                    <div class="mb-3 text-center">
                        <button class="btn btn-success btn-submit btn-sm">Guardar</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
     
</body>
  
<script type="text/javascript">
      
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
  
    $(".btn-submit").click(function(e){
    
        e.preventDefault();
     
        var nombre = $("#titleID").val();
        var brand_id = $("#bodyID").val();
     
        $.ajax({
           type:'POST',
           url:"{{ route('articulos.store') }}",
           data:{nombre:nombre, brand_id:brand_id},
           success:function(data){
                if($.isEmptyObject(data.error)){
                    alert(data.success);
                    location.reload();
                }else{
                    printErrorMsg(data.error);
                }
           }
        });
    });
  
    function printErrorMsg (msg) {
        $(".print-error-msg").find("ul").html('');
        $(".print-error-msg").css('display','block');
        $.each( msg, function( key, value ) {
            $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
        });
    }
  
</script>
</html>
