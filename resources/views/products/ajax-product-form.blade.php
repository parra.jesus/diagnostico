<!DOCTYPE html>
<html>
<head>
    <title>Form Submit using Ajax jQuery in Laravel 9 with Validation</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="hhttps://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.4/jquery.validate.min.js"></script>
  <style>
    .error{
     color: #FF0000; 
    }
  </style>
</head>
<body>
<div class="container mt-4">
  <div class="card">
    <div class="card-header text-center font-weight-bold">
      <h2>Laravel 9 Ajax Form Submit with Validation Example</h2>
    </div>
    <div class="card-body">
      <form name="ajax-product-form" id="ajax-product-form" method="post" action="javascript:void(0)">
       @csrf
        <div class="form-group">
          <label for="exampleInputEmail1">Name</label>
          <input type="text" id="name" name="nombre" class="form-control">
        </div>          
         <div class="form-group">
          <label for="exampleInputEmail1">Email</label>
          <input type="brandid" id="brandid" name="brand_id" class="form-control">
        </div>           
        <button type="submit" class="btn btn-primary" id="submit">Submit</button>
      </form>
    </div>
  </div>
</div>    
<script>
if ($("#ajax-product-form").length > 0) {
$("#ajax-product-form").validate({
  rules: {
    nombre: {
    required: true,
    maxlength: 50
  },
  brand_id: {
    required: true,
    maxlength: 50,
  },  
  },
  description: {
    required: "Please enter description",
    maxlength: "Your description name maxlength should be 300 characters long."
  },
  },
  submitHandler: function(form) {
  $.ajaxSetup({
    headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  $('#submit').html('Please Wait...');
  $("#submit"). attr("disabled", true);
  $.ajax({
    url: "{{url('save')}}",
    type: "POST",
    data: $('#ajax-product-form').serialize(),
    success: function( response ) {
      $('#submit').html('Submit');
      $("#submit"). attr("disabled", false);
      alert('Ajax form has been submitted successfully');
      document.getElementById("ajax-product-form").reset(); 
    }
   });
  }
  })
}
</script>
</body>
</html>
