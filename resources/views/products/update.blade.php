@extends('products.layout')

@section('content')
<div class="container">
	@if(session()->has('status'))
		<p class="alert alert-info">
			{{	session()->get('status') }}
		</p>
	@endif
    <div class="col-sm-6 col-sm-offset-3">
    	<div class="panel panel-default">
    		<div class="panel-heading">
    			Add User
    		</div>
    		<div class="panel-body">
				{{ Form::model($products, ['url' => route('products.store'), 'method' => 'PUT' ]) }}
					@include('users._form')
				{{ Form::close() }}
    		</div>
		</div>
	</div>
</div>
@endsection
